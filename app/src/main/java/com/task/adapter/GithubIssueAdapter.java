package com.task.adapter;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.task.R;
import com.task.activity.BaseActivity;
import com.task.holder.IssueItemHolder;
import com.task.roomdb.entity.GithubIssue;

import java.util.ArrayList;
import java.util.List;

public class GithubIssueAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    BaseActivity baseActivity;
    List<GithubIssue> arrayList = new ArrayList<>();
    public GithubIssueAdapter(BaseActivity baseActivity, List<GithubIssue> githubIssues){
        this.baseActivity = baseActivity;
        this.arrayList = githubIssues;
    }

    public GithubIssueAdapter(BaseActivity baseActivity){
        this.baseActivity = baseActivity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.github_issue_item_row, parent, false);

        IssueItemHolder issueItemHolder = new IssueItemHolder(itemView);
        return issueItemHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final GithubIssue githubIssue = arrayList.get(position);
        IssueItemHolder issueItemHolder = (IssueItemHolder) holder;

        issueItemHolder.titleTV.setText(githubIssue.getTitle());
        issueItemHolder.bodyTV.setText(githubIssue.getBody());

        issueItemHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                baseActivity.openDetailsActivity(baseActivity, githubIssue);
            }
        });

//        issueItemHolder.titleTV.setText("Title");
//        issueItemHolder.bodyTV.setText("Body");

    }

    public void setGitHubIssues(List<GithubIssue> gitHubIssuesListArr){
        arrayList.addAll(gitHubIssuesListArr);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(arrayList != null){
            return arrayList.size();
        }else{
            return 0;
        }

    }
}

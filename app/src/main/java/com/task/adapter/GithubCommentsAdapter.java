package com.task.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.task.AppUtils;
import com.task.GlideController;
import com.task.R;
import com.task.activity.BaseActivity;
import com.task.holder.CommentsHolder;
import com.task.holder.IssueItemHolder;
import com.task.roomdb.entity.GithubComments;
import com.task.roomdb.entity.GithubIssue;

import java.util.ArrayList;
import java.util.List;

public class GithubCommentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    BaseActivity baseActivity;
    ArrayList<GithubComments> arrayList = new ArrayList<>();
    public GithubCommentsAdapter(BaseActivity baseActivity, ArrayList<GithubComments> arrayList){
        this.baseActivity = baseActivity;
        this.arrayList = arrayList;
    }

    public GithubCommentsAdapter(BaseActivity baseActivity){
        this.baseActivity = baseActivity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_item_row, parent, false);

        CommentsHolder commentsHolder = new CommentsHolder(itemView);
        return commentsHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final GithubComments githubComments = arrayList.get(position);
        CommentsHolder commentsHolder = (CommentsHolder) holder;

        commentsHolder.bodyTV.setText(githubComments.getBody());
        commentsHolder.userName.setText(githubComments.getUser().getLogin());

        try{
            commentsHolder.createdAt.setText("Created At: "+ AppUtils.getInstance().convertTime(githubComments.getCreated_at()));
        }catch (Exception e){

        }

        GlideController.displayThumbnailRoundedImage(baseActivity, githubComments.getUser().avatar_url, commentsHolder.userImage);

    }

    @Override
    public int getItemCount() {
        if(arrayList != null){
            return arrayList.size();
        }else{
            return 0;
        }
    }

    public void setCommentsData(List<GithubComments> githubCommentsListArr){
        arrayList.addAll(githubCommentsListArr);
        notifyDataSetChanged();

    }
}

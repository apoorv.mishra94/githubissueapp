package com.task.repository;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.task.AppApplication;
import com.task.AppUtils;
import com.task.activity.BaseActivity;
import com.task.retrofit.GetDataInterface;
import com.task.interfaces.ApiUrl;
import com.task.retrofit.RetrofitService;
import com.task.retrofit.WebServiceExecutor;
import com.task.roomdb.DatabaseClient;
import com.task.roomdb.daopkg.GithubIssueDao;
import com.task.roomdb.entity.GithubIssue;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class GithubIssueRepo implements WebServiceExecutor.APIResponseListener{

    private static GithubIssueRepo instance;
    //private ArrayList<GithubIssue> mGithubIssuesList = new ArrayList<>();

    private MutableLiveData<List<GithubIssue>> mGithubIssuesList;
    private MutableLiveData<Boolean> isLoading;

    BaseActivity baseActivity;
    public static GithubIssueRepo getInstance(BaseActivity baseActivity){
        if(instance == null){
            instance = new GithubIssueRepo(baseActivity);
        }
        return instance;
    }
    GithubIssueDao mGithubIssueDao;
    private GithubIssueRepo(BaseActivity baseActivity){
        this.baseActivity = baseActivity;
        mGithubIssuesList = new MutableLiveData<>();
        isLoading = new MutableLiveData<>();
//        List<GithubIssue> githubIssuesArrList = new ArrayList<>();
//        mGithubIssuesList.postValue(githubIssuesArrList);
       mGithubIssueDao =  DatabaseClient.getInstance(baseActivity).getAppDatabase().githubIssueDao();
    }

    // Pretend to get data from a webservice or online source
    public MutableLiveData<List<GithubIssue>> getGitHubIsues(){
        getGithubIssuesData();
        return mGithubIssuesList;
    }

    public MutableLiveData<Boolean> isLoading(){

        return isLoading;
    }

    private void getGithubIssuesData(){
        if(AppUtils.getInstance().isNetworkEnabled(AppApplication.getInstance()) && AppUtils.getInstance().shouldPullDataFromServer(baseActivity)){
            callGithubIssueApi(baseActivity, "https://api.github.com/repos/firebase/firebase-ios-sdk/issues");
        }else{
            getGitHubIssueRoomDB();
        }
    }

    public void callGithubIssueApi(BaseActivity context, String url) {
        try {
            Call<ResponseBody> call = RetrofitService.getInstance().builder(ApiUrl.baseUrl).create(GetDataInterface.class).getGithubIssueCall(url);
            WebServiceExecutor.getInstance().execute(context, null, call, this, WebServiceExecutor.GITHUB_ISSUE_API_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getGitHubIssueRoomDB() {
        isLoading.postValue(true);
        class GetIssueAsync extends AsyncTask<Void, Void, List<GithubIssue>> {

            @Override
            protected List<GithubIssue> doInBackground(Void... voids) {
                List<GithubIssue> githubIssuesList = mGithubIssueDao.getAllIssues();
                return githubIssuesList;
            }

            @Override
            protected void onPostExecute(List<GithubIssue> githubIssuesList) {
                super.onPostExecute(githubIssuesList);
                isLoading.postValue(false);
                if(mGithubIssuesList.getValue() != null && mGithubIssuesList.getValue().size() > 0){
                    mGithubIssuesList.getValue().clear();
                }
                mGithubIssuesList.postValue(githubIssuesList);
            }
        }

        GetIssueAsync gt = new GetIssueAsync();
        gt.execute();
    }

    private void setGitHubIssueRoomDB(final BaseActivity baseActivity, final ArrayList<GithubIssue> arrayList){

        class SetIssueAsync extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                mGithubIssueDao.deleteAll();

                //adding to database
                mGithubIssueDao.insert(arrayList);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }

        SetIssueAsync st = new SetIssueAsync();
        st.execute();
    }

    @Override
    public void onLoading(boolean isLoadingBool, Bundle bundle) {
        if(isLoadingBool){
            isLoading.postValue(true);
        }else{
            isLoading.postValue(false);
        }
    }

    @Override
    public void onNetworkSuccess(Call call, Response response, Bundle bundle, int requestType) {
        isLoading.postValue(false);
        if (requestType == WebServiceExecutor.GITHUB_ISSUE_API_REQUEST) {
            try {
                String jsonData = ((ResponseBody) response.body()).string();
                Log.d("teststring22222", jsonData);

                Type listType = new TypeToken<ArrayList<GithubIssue>>() {
                }.getType();

                ArrayList<GithubIssue> arrayList = new Gson().fromJson(jsonData, listType);
                setGitHubIssueRoomDB(baseActivity, arrayList);
                AppUtils.getInstance().setLastApiHitTime(baseActivity);
                if(mGithubIssuesList.getValue() != null && mGithubIssuesList.getValue().size() > 0){
                    mGithubIssuesList.getValue().clear();
                }
                mGithubIssuesList.postValue(arrayList);

            } catch (Exception e) {
                e.printStackTrace();
                //homeActivity.setError();
            }
        }
    }

    @Override
    public void onNetworkFailure(Call call, Response response, Throwable throwable, Bundle bundle, int requestType) {
        isLoading.postValue(false);
    }

    @Override
    public void onNetworkFailureAndError(Call call, Response response, Object o, Bundle bundle, WebServiceExecutor.APIResponseListener listener, int requestType) {
        isLoading.postValue(false);
    }
}

package com.task.repository;

import android.os.Bundle;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.task.activity.BaseActivity;
import com.task.retrofit.GetDataInterface;
import com.task.interfaces.ApiUrl;
import com.task.retrofit.RetrofitService;
import com.task.retrofit.WebServiceExecutor;
import com.task.roomdb.DatabaseClient;
import com.task.roomdb.daopkg.GithubCommentDao;
import com.task.roomdb.entity.GithubComments;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class GithubIssueCommentRepo implements WebServiceExecutor.APIResponseListener{

    private static GithubIssueCommentRepo instance;
    BaseActivity baseActivity;
    private MutableLiveData<List<GithubComments>> mGithubIssuesCommentList;
    GithubCommentDao mGithubIssueDao;

    public static GithubIssueCommentRepo getInstance(BaseActivity baseActivity){
        if(instance == null){
            instance = new GithubIssueCommentRepo(baseActivity);
        }
        return instance;
    }

    public GithubIssueCommentRepo(BaseActivity baseActivity){
        this.baseActivity = baseActivity;
        mGithubIssuesCommentList = new MutableLiveData<>();
        //mGithubIssueDao =  DatabaseClient.getInstance(baseActivity).getAppDatabase().githubIssueCommentDao();
    }

    // Pretend to get data from a webservice or online source
    public MutableLiveData<List<GithubComments>> getGitHubIsuesComment(String commentUrl){

        //getGitHubIssueRoomDB();

        callGetCommentS(baseActivity, commentUrl);


        //callGithubIssueApi(baseActivity, "https://api.github.com/repos/firebase/firebase-ios-sdk/issues");
        return mGithubIssuesCommentList;
    }

    public void callGetCommentS(BaseActivity baseActivity, String url){
        try {
            Call<ResponseBody> call = RetrofitService.getInstance().builder(ApiUrl.baseUrl).create(GetDataInterface.class).getGithubIssueCall(url);
            WebServiceExecutor.getInstance().execute(baseActivity, null, call, this, WebServiceExecutor.GITHUB_ISSUE_COMMENT_API_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLoading(boolean isLoading, Bundle bundle) {

    }

    @Override
    public void onNetworkSuccess(Call call, Response response, Bundle bundle, int requestType) {
        if (requestType == WebServiceExecutor.GITHUB_ISSUE_COMMENT_API_REQUEST) {
            try {
                String jsonData = ((ResponseBody) response.body()).string();
                Log.d("teststring22222", jsonData);

                Type listType = new TypeToken<ArrayList<GithubComments>>() {
                }.getType();
                ArrayList<GithubComments> arrayList = new Gson().fromJson(jsonData, listType);
                if(mGithubIssuesCommentList.getValue() != null && mGithubIssuesCommentList.getValue().size() > 0){
                    mGithubIssuesCommentList.getValue().clear();
                }
                mGithubIssuesCommentList.postValue(arrayList);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onNetworkFailureAndError(Call call, Response response, Object o, Bundle bundle, WebServiceExecutor.APIResponseListener listener, int requestType) {

    }

    @Override
    public void onNetworkFailure(Call call, Response response, Throwable throwable, Bundle bundle, int requestType) {

    }
}

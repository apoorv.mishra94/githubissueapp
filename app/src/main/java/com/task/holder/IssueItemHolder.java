package com.task.holder;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.task.R;
import com.task.view.GithubTextView;

public class IssueItemHolder extends RecyclerView.ViewHolder {

    public GithubTextView titleTV;
    public GithubTextView bodyTV;

    public  IssueItemHolder(View itemView){
        super(itemView);
        titleTV = (GithubTextView) itemView.findViewById(R.id.titleTV);
        bodyTV = (GithubTextView) itemView.findViewById(R.id.bodyTV);
    }

}

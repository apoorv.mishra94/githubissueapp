package com.task.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.task.R;
import com.task.view.GithubTextView;

public class CommentsHolder extends RecyclerView.ViewHolder {

    public GithubTextView bodyTV, createdAt, userName;
    public ImageView userImage;

    public CommentsHolder(View itemView){
        super(itemView);
        bodyTV = (GithubTextView) itemView.findViewById(R.id.bodyTV);
        userName = (GithubTextView) itemView.findViewById(R.id.userName);
        createdAt = (GithubTextView) itemView.findViewById(R.id.createdAt);
        userImage = (ImageView) itemView.findViewById(R.id.userImage);

    }

}

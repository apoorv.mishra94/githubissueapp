package com.task;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.task.interfaces.Constant;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AppUtils {

    private static AppUtils instance;

    private AppUtils() {

    }

    public static AppUtils getInstance() {
        if (instance == null) {
            synchronized (AppUtils.class) {
                if (instance == null) {
                    instance = new AppUtils();
                }
            }
        }
        return instance;
    }

    /**
     * Check internet connection on device.
     * <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" /> <br/>
     * <uses-permission android:name="android.permission.READ_PHONE_STATE" />
     *
     * @param pContext activities/fragment contexts
     * @return true/false
     */
    public boolean isNetworkEnabled(Context pContext) {
        if (pContext != null) {
            try {
                ConnectivityManager conMngr = (ConnectivityManager) pContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = conMngr != null ? conMngr.getActiveNetworkInfo() : null;
                return activeNetwork != null && activeNetwork.isConnected();
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }

    public boolean shouldPullDataFromServer(Context context){
        boolean pullDataFromServer = false;
        long currentTime = System.currentTimeMillis();
        long lastApiHitTime = getLastApiHitTime(context);
        if((currentTime - lastApiHitTime) > Constant.TIME_FOR_NEXT_API_HIT){
            pullDataFromServer = true;
        }
        return pullDataFromServer;
    }

    public void setLastApiHitTime(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEdit = sharedPreferences.edit();
        prefEdit.putLong(Constant.LAST_ISSUE_API_HITP_TIME, System.currentTimeMillis());
        prefEdit.commit();
    }

    public Long getLastApiHitTime(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("", Context.MODE_PRIVATE);
        return sharedPreferences.getLong(Constant.LAST_ISSUE_API_HITP_TIME, 0);
    }

    public String convertTime(String dateString){
        String timeAgo = "" ;
        try{

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

            Date convertedDate = new Date();

            try {
                convertedDate = dateFormat.parse(dateString);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            PrettyTime p  = new PrettyTime();

            timeAgo = p.format(convertedDate);

        }catch (Exception e){

        }
        return timeAgo;
    }

}

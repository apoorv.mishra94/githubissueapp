package com.task;

import androidx.multidex.MultiDexApplication;

public class AppApplication extends MultiDexApplication {

    private static AppApplication appInstance = null;
    public static AppApplication getInstance() {
        return appInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appInstance = this;

    }

}

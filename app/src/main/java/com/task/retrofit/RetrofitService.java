package com.task.retrofit;


import androidx.annotation.NonNull;

import com.task.interfaces.ApiUrl;
import com.task.AppApplication;
import com.task.AppUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitService {

    private static RetrofitService instance;
    private OkHttpClient okHttpClient;
    private Retrofit retrofit;
    private OkHttpClient okHttpClientCustom;
    private Retrofit retrofitCustom;
    private HttpLoggingInterceptor interceptor;
    private int READ_TIME_OUT = 40000;
    private int CONNECTION_TIME_OUT = 40000;

    private RetrofitService() {

    }

    public static RetrofitService getInstance() {
        if (instance == null) {
            synchronized (RetrofitService.class) {
                instance = new RetrofitService();
            }
        }
        return instance;
    }

    /**
     * Get Default instance.
     *
     * @return Retrofit object
     */
    public Retrofit builder() {
        return getRetrofit(getOkHttpClient());
    }

    /**
     * Get Default instance with custom url.
     *
     * @param url webservice url.
     * @return Retrofit object
     */
    public Retrofit builder(String url) {
        return getRetrofit(getOkHttpClient(), url);
    }

    /**
     * Get Custom instance with readTimeout and connectTimeout with default.
     *
     * @param readTimeout    readTimeout in long
     * @param connectTimeout connectTimeout in long
     * @return Retrofit object.
     */
    public Retrofit builder(long readTimeout, long connectTimeout) {
        return getRetrofit(getOkHttpClient(readTimeout, connectTimeout));
    }

    /**
     * Get Custom instance with readTimeout and connectTimeout custom url.
     *
     * @param url            webservice url.
     * @param readTimeout    readTimeout in long
     * @param connectTimeout connectTimeout in long
     * @return Retrofit object.
     */
    public Retrofit builder(String url, long readTimeout, long connectTimeout) {
        return getRetrofit(getOkHttpClient(readTimeout, connectTimeout), url);
    }


    private OkHttpClient getOkHttpClient() {
        if (okHttpClient == null) {
            synchronized (RetrofitService.class) {
                okHttpClient = new OkHttpClient.Builder()
                        .readTimeout(READ_TIME_OUT, TimeUnit.MILLISECONDS)
                        .connectTimeout(CONNECTION_TIME_OUT, TimeUnit.MILLISECONDS)
                        .cache(setCacheSupport())
                        .addInterceptor(setInterceptor())
                        .addNetworkInterceptor(getLoggingInterceptor())
                        .build();
            }
        }
        return okHttpClient;
    }

    private OkHttpClient getOkHttpClient(long readTimeout, long connectTimeout) {
        if (okHttpClientCustom == null) {
            synchronized (RetrofitService.class) {
                okHttpClientCustom = new OkHttpClient.Builder()
                        .readTimeout(readTimeout, TimeUnit.MILLISECONDS)
                        .connectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
                        .cache(setCacheSupport())
                        .addInterceptor(setInterceptor())
                        .addNetworkInterceptor(getLoggingInterceptor())
                        .build();
            }
        }
        return okHttpClientCustom;
    }

    private Retrofit getRetrofit(OkHttpClient okHttpClient) {
        if (retrofit == null) {
            synchronized (RetrofitService.class) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(ApiUrl.baseUrl)
                        .client(okHttpClient)
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
        }
        return retrofit;
    }
    //vkb.addConverterFactory(GsonConverterFactory.create())
    private Retrofit getRetrofit(OkHttpClient okHttpClient, String URL) {
        if (retrofitCustom == null) {
            synchronized (RetrofitService.class) {
                retrofitCustom = new Retrofit.Builder()
                        .baseUrl(URL)
                        .client(okHttpClient)
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
        }
        return retrofitCustom;
    }

    private HttpLoggingInterceptor getLoggingInterceptor() {
        if (interceptor == null) {
            synchronized (RetrofitService.class) {
                interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
            }
        }
        return interceptor;
    }


    private Cache setCacheSupport() {
        int cacheSize = 10 * 1024 * 1024; // 10 MB
        return new Cache(AppApplication.getInstance().getCacheDir(), cacheSize);
    }

    private Interceptor setInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                Request request = chain.request();
                if (AppUtils.getInstance().isNetworkEnabled(AppApplication.getInstance())) {
                    int maxAge = 60; // read from cache for 1 minute
                    request = request.newBuilder().header("Cache-Control", "public, max-age=" + maxAge).build();
                } else {
                    // int maxStale = 60  60  24 * 7; // tolerate 1-weeks stale;
                    int maxStale = 60 * 60 * 24 * 28; // tolerate 4-weeks stale;
                    request = request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale).build();
                }
                return chain.proceed(request);
            }
        };
    }

}

package com.task.retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface GetDataInterface {

    @GET
    Call<ResponseBody> getGithubIssueCall(@Url String url);

}

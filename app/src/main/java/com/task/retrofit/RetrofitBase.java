package com.task.retrofit;

import android.os.Bundle;

import androidx.annotation.NonNull;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class RetrofitBase implements Callback<ResponseBody> {

    private final Call<ResponseBody> call;
    private Bundle bundle;
    private int totalRetries;
    private int retryCount = 0;

    protected RetrofitBase(Call<ResponseBody> call, int totalRetries, Bundle bundle) {
        this.call = call;
        this.totalRetries = totalRetries;
        this.bundle = bundle;
    }

    @Override
    public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
        try {
            if (!isCallSuccess(response)) {
                if (retryCount++ < totalRetries) {
                    retry();
                } else {
                    onNetworkSuccess(call, response, bundle);
                }
            } else {
                onNetworkSuccess(call, response, bundle);
            }
        } catch (Exception e) {
            onNetworkError(call, e, bundle);
        }
    }

    @Override
    public void onFailure(@NonNull Call call, @NonNull Throwable ResponseBody) {
        try {
            if (retryCount++ < totalRetries) {
                retry();
            } else {
                onNetworkFailure(call, ResponseBody, bundle);
            }
        } catch (Exception e) {
            onNetworkError(call, e, bundle);
        }
    }

    protected abstract void onNetworkSuccess(Call<ResponseBody> call, Response<ResponseBody> response, Bundle bundle) throws Exception;

    protected abstract void onNetworkFailure(Call<ResponseBody> call, Throwable ResponseBody, Bundle bundle) throws Exception;

    protected abstract void onNetworkError(Call<ResponseBody> call, Throwable error, Bundle bundle);

    private void retry() {
        call.clone().enqueue(this);
    }

    private boolean isCallSuccess(Response response) {
        int code = response.code();
        return (code >= 200 && code < 400);
    }

}

package com.task.retrofit;

import android.os.Bundle;

import com.task.activity.BaseActivity;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class WebServiceExecutor {

    public static final int GITHUB_ISSUE_API_REQUEST = 1;
    public static final int GITHUB_ISSUE_COMMENT_API_REQUEST = 2;

    public final String REQUEST_URL_TAG = "reqUrlTag";
    private int DEFAULT_RETRY_COUNT = 0;
    private Map<String, Call> manageCancelMap;
    private static WebServiceExecutor instance;
    public static WebServiceExecutor getInstance() {
        if (instance == null) {
            synchronized (WebServiceExecutor.class) {
                if (instance == null) {
                    instance = new WebServiceExecutor();
                }
            }
        }
        return instance;
    }

    public WebServiceExecutor execute(BaseActivity activity, final Bundle bundle, Call<ResponseBody> call, APIResponseListener<ResponseBody> listener, int requestType) {
        return execute(activity, bundle, call, DEFAULT_RETRY_COUNT, listener, requestType);
    }

    public WebServiceExecutor execute(BaseActivity activity, final Bundle bundle, Call<ResponseBody> call, int retryCount, final APIResponseListener<ResponseBody> listener, final int requestType) {
        if (activity != null && call != null) {
            if (activity.showProgressBar)
                showOrHideProgressDialog(true, bundle, listener);
            setServiceCallInMap(call, bundle);
            call.enqueue(new RetrofitBase(call, retryCount, bundle) {
                @Override
                public void onNetworkSuccess(final Call<ResponseBody> call, final Response<ResponseBody> response, final Bundle bundle) {
                    showOrHideProgressDialog(false, bundle, listener);
                    if (response != null && response.body() != null && listener != null) {
                        Response<ResponseBody> responset = response;
                        listener.onNetworkSuccess(call, responset, bundle, requestType);
                    } else {
                        listener.onNetworkFailureAndError(call, response, null, bundle, listener, requestType);
                    }
                    resetServiceCallInMap(bundle);
                }

                @Override
                public void onNetworkFailure(Call<ResponseBody> call, Throwable t, Bundle bundle) {
                    showOrHideProgressDialog(false, bundle, listener);
                    onNetworkFailureAndError(call, null, t, bundle, listener, requestType);
                    resetServiceCallInMap(bundle);
                }

                @Override
                public void onNetworkError(Call<ResponseBody> call, Throwable t, Bundle bundle) {
                    showOrHideProgressDialog(false, bundle, listener);
                    onNetworkFailureAndError(call, null, t, bundle, listener, requestType);
                    resetServiceCallInMap(bundle);
                }
            });
        } else {
            showOrHideProgressDialog(false, bundle, listener);
        }
        return instance;
    }

    private <RequestModel> void showOrHideProgressDialog(boolean isLoading, Bundle bundle, APIResponseListener<RequestModel> listener) {
        if (listener != null) {
            listener.onLoading(isLoading, bundle);
        }
    }

    private <RequestModel> void onNetworkFailureAndError(Call<RequestModel> call, Response<RequestModel> response, Throwable t, Bundle bundle, APIResponseListener<RequestModel> listener, int requestType) {
        if (call != null)
            call.cancel();
        if (listener != null)
            listener.onNetworkFailure(call, response, t, bundle, requestType);
    }


    private <RequestModel> void setServiceCallInMap(Call<RequestModel> call, Bundle bundle) {
        if (manageCancelMap != null && bundle != null && bundle.containsKey(REQUEST_URL_TAG)) {
            manageCancelMap.put(bundle.getString(REQUEST_URL_TAG), call);
        }
    }

    private void resetServiceCallInMap(Bundle bundle) {
        if (manageCancelMap != null && manageCancelMap.size() > 0 && bundle != null && bundle.containsKey(REQUEST_URL_TAG)) {
            manageCancelMap.remove(bundle.getString(REQUEST_URL_TAG));
        }
    }

    public interface APIResponseListener<RequestModel> {

        void onLoading(boolean isLoading, Bundle bundle);

        void onNetworkSuccess(Call<RequestModel> call, Response<RequestModel> response, Bundle bundle, int requestType);

        void onNetworkFailure(Call<RequestModel> call, Response<RequestModel> response, Throwable throwable, Bundle bundle, int requestType);

        void onNetworkFailureAndError(Call<RequestModel> call, Response<RequestModel> response, Object o, Bundle bundle, APIResponseListener<RequestModel> listener, int requestType);
    }

}


package com.task.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.core.widget.TextViewCompat;

import org.w3c.dom.Text;

public class GithubTextView extends TextView {



    public GithubTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFontFaceType(context, attrs);
//        setTypeface(ZeeNewsApplication.getInstance().droidTypeFace, Typeface.NORMAL);

    }

    private void setFontFaceType(Context context, AttributeSet attrs){
        setTypeface( Typeface.createFromAsset(context.getAssets(), "nunito_sans_regular.ttf"), Typeface.NORMAL);
    }

}

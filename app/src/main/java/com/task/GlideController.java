package com.task;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

public class GlideController {

    private final static String TAG = "GlideController-->>";

    public static void displayThumbnailRoundedImage(final Context context, String url, final ImageView imageView) {
//        Glide.with(context)
//                .load(url)
//                .asBitmap()
//                .centerCrop()
//                .placeholder(R.drawable.default_author)
//                .error(R.drawable.default_author)
//                .thumbnail(0.1f)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(new BitmapImageViewTarget(imageView) {
//                    @Override
//                    protected void setResource(Bitmap resource) {
//                        try {
//                            RoundedBitmapDrawable circularBitmapDrawable =
//                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
//                            circularBitmapDrawable.setCornerRadius(16);
//                            imageView.setImageDrawable(circularBitmapDrawable);
//                        }catch (OutOfMemoryError error){
//                            AppLog.e(TAG, "setResource: ", error);
//                        }
//                    }
//                });


        Glide.with(context)
                .load(url)
                .placeholder(R.drawable.default_author)
                .error(R.drawable.default_author)
                .apply(RequestOptions.circleCropTransform())
                .into(imageView);
    }

}

package com.task;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class AppLog {
    private static boolean isBuildDebug = true;

    static {
        if (BuildConfig.DEBUG){
            isBuildDebug = true;
        }else {
            isBuildDebug = false;
        }
    }

    public static void d(String TAG, String msg){
        if (isBuildDebug){
            Log.d(TAG, "d: -->> "+msg);
        }
    }

    public static void e(String TAG, String msg, Throwable e){
        if (isBuildDebug){
            Log.e(TAG, "e: -->> "+msg, e);
        }
    }

    public static void e(String TAG, String msg){
        if (isBuildDebug){
            Log.e(TAG, "e: -->> "+msg);
        }
    }

    public static void w(String TAG, String msg){
        if (isBuildDebug){
            Log.w(TAG, "w: -->> "+msg);
        }
    }

    public static void i(String TAG, String msg){
        if (isBuildDebug){
            Log.w(TAG, "i: -->> "+msg);
        }
    }

    public static void Toast(Context context, String msg){
        if (isBuildDebug){
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
        }
    }
}


package com.task.interfaces;

public interface Constant {

    String LAST_ISSUE_API_HITP_TIME = "last_issue_api_hit_time";
    String LAST_COMMENT_API_HITP_TIME = "last_comment_api_hit_time";

    long TIME_FOR_NEXT_API_HIT = 24 * 60 * 60;// 24 hours

}

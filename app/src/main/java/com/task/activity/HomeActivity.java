package com.task.activity;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.task.viewmodel.HomeActivityViewModel;
import com.task.R;
import com.task.adapter.GithubIssueAdapter;
import com.task.roomdb.entity.GithubIssue;

import java.util.List;

public class HomeActivity extends BaseActivity {

    HomeActivityViewModel homeActivityViewModel;
    GithubIssueAdapter mAdapter;
    RecyclerView mRecyclerView;
    BaseActivity baseActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        baseActivity = this;

        initViews();

        homeActivityViewModel = ViewModelProviders.of(this).get(HomeActivityViewModel.class);

        homeActivityViewModel.init(baseActivity);

        homeActivityViewModel.getGitHubIsues().observe(this, new Observer<List<GithubIssue>>() {
            @Override
            public void onChanged(@Nullable List<GithubIssue> githubIssues) {
                mAdapter.setGitHubIssues(githubIssues);
            }
        });

        homeActivityViewModel.isLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(aBoolean){
                    baseActivity.showProgressDialog("");
                }else{
                    baseActivity.hideProgressDialog();
                }
            }
        });

        initRecyclerView();

    }

    private void initViews(){
        mRecyclerView = (RecyclerView) findViewById(R.id.recycleView);
    }

    private void initRecyclerView(){
        mAdapter = new GithubIssueAdapter(baseActivity);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(baseActivity, RecyclerView.VERTICAL, false));
        mRecyclerView.setAdapter(mAdapter);
    }

    public void setError(){
        baseActivity.hideProgressDialog();
    }
}

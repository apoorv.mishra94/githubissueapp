package com.task.activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;

import com.task.AppUtils;
import com.task.viewmodel.DetailsActivityViewModel;
import com.task.GlideController;
import com.task.R;
import com.task.adapter.GithubCommentsAdapter;
import com.task.roomdb.entity.GithubComments;
import com.task.roomdb.entity.GithubIssue;
import com.task.view.GithubTextView;

import java.util.ArrayList;
import java.util.List;

public class DetailsActivity extends BaseActivity {
    GithubIssue githubIssue;
    GithubTextView title, body, userName, createdTime;
    ImageView userImage;
    BaseActivity baseActivity;
    DetailsActivityViewModel detailsActivityViewModel;
    GithubCommentsAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        baseActivity = this;
        setTitle("GitHub Issue");
        githubIssue = (GithubIssue) getIntent().getSerializableExtra("github_issue_data");
        initializeViews();

        if(githubIssue != null){
            setData();

            detailsActivityViewModel = ViewModelProviders.of(this).get(DetailsActivityViewModel.class);

            detailsActivityViewModel.init(baseActivity, githubIssue.getComments_url());


            detailsActivityViewModel.getGitHubIsuesComments(githubIssue.getComments_url()).observe(this, new Observer<List<GithubComments>>() {
                @Override
                public void onChanged(List<GithubComments> githubComments) {
                    mAdapter.setCommentsData(githubComments);
                }
            });
            initRecyclerView();

        }
    }

    RecyclerView mRecyclerView;
    private void initializeViews(){
        title = (GithubTextView) findViewById(R.id.titleTV);
        body = (GithubTextView) findViewById(R.id.bodyTV);
        userName = (GithubTextView) findViewById(R.id.userName);
        createdTime = (GithubTextView) findViewById(R.id.createdTime);
        userImage = (ImageView) findViewById(R.id.userImage);

        mRecyclerView = (RecyclerView) findViewById(R.id.commentsRCV);
    }

    private void initRecyclerView(){
        mAdapter = new GithubCommentsAdapter(baseActivity);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(baseActivity, RecyclerView.VERTICAL, false));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setData(){
        title.setText(githubIssue.getTitle());
        body.setText(githubIssue.getBody());
        userName.setText(githubIssue.getUser().getLogin());
        try{
            createdTime.setText(AppUtils.getInstance().convertTime(githubIssue.getCreated_at()));
        }catch (Exception e){
            e.printStackTrace();
        }

        GlideController.displayThumbnailRoundedImage(baseActivity, githubIssue.getUser().avatar_url, userImage);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        else {
            return super.onOptionsItemSelected(item);
        }
    }
}

package com.task.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ScrollView;

import androidx.appcompat.app.AppCompatActivity;

import com.task.AppLog;
import com.task.R;
import com.task.roomdb.entity.GithubIssue;

public class BaseActivity extends AppCompatActivity {

    final String TAG = "BaseActivity";

    private ProgressDialog progressDialog;
    public boolean showProgressBar = true;

    protected void showProgressDialog(String dialogMessage) {
        if(progressDialog != null && progressDialog.isShowing()) {
            progressDialog.hide();
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(dialogMessage);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void openDetailsActivity(Context context, GithubIssue githubIssue){
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra("github_issue_data", githubIssue);
        startActivity(intent);
    }

    public void startActivity(Intent intent) {
        super.startActivity(intent);
        try {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        } catch (Exception e) {
            AppLog.e(TAG, "startActivity: ", e);
        }
    }

    @Override
    public void onBackPressed() {
        performBackAction(null);
    }

    /**
     * Perform back button action.
     *
     * @param v
     */
    public void performBackAction(View v) {
        try {
            super.onBackPressed();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } catch (Exception e) {
            AppLog.e(TAG, "performBackAction: ", e);
        }
    }

    protected void hideProgressDialog() {
        if(progressDialog != null && progressDialog.isShowing()) {
            progressDialog.hide();
        }
    }

    protected void scrollToBottom(final ScrollView scroll){
        scroll.post(new Runnable() {
            @Override
            public void run() {
                scroll.fullScroll(View.FOCUS_DOWN);
            }
        });
    }
}
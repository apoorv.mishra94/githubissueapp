package com.task.roomdb;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.task.roomdb.daopkg.GithubCommentDao;
import com.task.roomdb.daopkg.GithubIssueDao;
import com.task.roomdb.entity.GithubIssue;

@Database(entities = {GithubIssue.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract GithubIssueDao githubIssueDao();

    //public abstract GithubCommentDao githubIssueCommentDao();
}

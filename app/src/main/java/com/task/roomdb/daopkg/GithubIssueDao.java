package com.task.roomdb.daopkg;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Delete;

import com.task.roomdb.entity.GithubIssue;

import java.util.List;

@Dao
public interface GithubIssueDao {

    @Query("Select * from GithubIssue")
    List<GithubIssue> getAllIssues();

    @Insert
    void insert(List<GithubIssue> arrGitHubIssueList);

    @Query("DELETE FROM GithubIssue")
    void deleteAll();

}

package com.task.roomdb.daopkg;

import androidx.room.Dao;
import androidx.room.Query;

import com.task.roomdb.entity.GithubComments;

import java.util.List;

@Dao
public interface GithubCommentDao {

    @Query("Select * from GithubComments")
    List<GithubComments> getAllComments();

}

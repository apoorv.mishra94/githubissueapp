package com.task.viewmodel;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.task.activity.BaseActivity;
import com.task.repository.GithubIssueRepo;
import com.task.roomdb.entity.GithubIssue;

import java.util.List;

public class HomeActivityViewModel extends ViewModel {

    GithubIssueRepo mRepo;
    MutableLiveData<List<GithubIssue>> mGithubIssue;
    MutableLiveData<Boolean> isLoading;

    public void init(BaseActivity baseActivity){
        if(mGithubIssue != null){
            return;
        }
        mRepo = GithubIssueRepo.getInstance(baseActivity);
        mGithubIssue = mRepo.getGitHubIsues();

        isLoading = mRepo.isLoading();
    }

    public LiveData<List<GithubIssue>> getGitHubIsues(){
        return mGithubIssue;
    }

    public LiveData<Boolean> isLoading(){
        return isLoading;
    }

}

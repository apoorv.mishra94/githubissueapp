package com.task.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.task.activity.BaseActivity;
import com.task.repository.GithubIssueCommentRepo;
import com.task.repository.GithubIssueRepo;
import com.task.roomdb.daopkg.GithubCommentDao;
import com.task.roomdb.entity.GithubComments;
import com.task.roomdb.entity.GithubIssue;

import java.util.List;

public class DetailsActivityViewModel extends ViewModel {

    GithubIssueCommentRepo mRepo;
    MutableLiveData<List<GithubComments>> mGithubIssuesCommentList;

    public void init(BaseActivity baseActivity, String commentUrl){
        if(mGithubIssuesCommentList != null){
            return;
        }
        mRepo = GithubIssueCommentRepo.getInstance(baseActivity);
        mGithubIssuesCommentList = mRepo.getGitHubIsuesComment(commentUrl);
    }

    public LiveData<List<GithubComments>> getGitHubIsuesComments(String commentUrl){
        return mGithubIssuesCommentList;
    }

}
